import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import './AddButt.css';

function AddButt({ onSubmit, text, wrapperClass }) {

    const parentNode = useRef();
    const inputRef = useRef();

    const [isOpen, setIsOpen] = useState(false);
    const [title, setTitle] = useState('');

    const handleClick = ({ target }) => {

        if (parentNode.current) {

            if (parentNode.current.contains(target))
                return

            setIsOpen(false);
        }
    }

    const addNewListHandler = e => {

        e.preventDefault();

        if (title) {
            onSubmit(title);
            setTitle('');
        }

    }

    useEffect(() => {

        if (isOpen) {
            document.addEventListener('mousedown', handleClick);
            inputRef.current.focus();
        }

        else
            document.removeEventListener('mousedown', handleClick);

    }, [isOpen])


    return (
        <div
            ref={parentNode}
            className={"g-add-input " + wrapperClass}
        >

            {
                isOpen ?
                    <form onSubmit={addNewListHandler}>
                        <input
                            className="g-add-input__input"
                            value={title}
                            onChange={({ target }) => setTitle(target.value)}
                            placeholder="Title..."
                            ref={inputRef}
                        />
                    </form> :
                    <div
                        className="g-add-input__butt"
                        onClick={() => setIsOpen(true)}
                    >
                        {text}
                    </div>
            }

            {
                isOpen &&
                <div className="g-add-input__butts">

                    <button
                        className="butt__pink-br"
                        onClick={addNewListHandler}
                    >
                        Add
                    </button>

                    <button
                        className="butt__pink-br"
                        onClick={() => setIsOpen(false)}
                    >
                        Cancel
                    </button>
                </div>
            }

        </div>
    )
}

AddButt.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    wrapperClass: PropTypes.string,
    text: PropTypes.string.isRequired
}

export default React.memo(AddButt);