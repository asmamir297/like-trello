import React from 'react';
import { connect } from 'react-redux';
import { clearSnackbar } from '../../store/snackbar'
import Snackbar from '@material-ui/core/Snackbar';


function Toaster({ isOpen, msg, clearSnackbar }) {

    const handleClose = () => {
        clearSnackbar();
    };

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            open={isOpen}
            autoHideDuration={6000}
            onClose={handleClose}
            message={msg}
        />
    )

}

const mapStateToProps = state => ({
    isOpen: state.snackbar.isOpen,
    msg: state.snackbar.msg
});

export default connect(mapStateToProps, { clearSnackbar })(Toaster)