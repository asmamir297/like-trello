import React, { useState, useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import TextField from "@material-ui/core/TextField";
import './EditorInput.css';


const useStyles = makeStyles(() => ({
    root: {
        '& .MuiTextField-root': {
            width: '100%',
            position: "relative",
            zIndex: "2",
            backgroundColor: 'white'
        },
    },
}));


function EditorInput({ val, editHandler }) {

    const classes = useStyles();
    const parentNode = useRef();

    const [value, setValue] = useState(val);
    const [isEditingMode, setIsEditingMode] = useState(false);

    const saveChanges = useCallback(() => {

        setIsEditingMode(false);

        if (value && value !== val)
            return editHandler(value);

        setValue(val);

    }, [val, value, editHandler])

    const handler = ({ target }) => {

        if (parentNode.current) {

            if (parentNode.current.contains(target))
                return

            setIsEditingMode(false);
        }
    }

    useEffect(() => {

        if (isEditingMode)
            document.addEventListener('mousedown', handler);

        else {
            saveChanges();
            document.removeEventListener('mousedown', handler)
        }

    }, [isEditingMode, saveChanges])


    if (isEditingMode)
        return (
            <form
                ref={parentNode}
                className={classes.root}
                noValidate
                autoComplete="off"
            >
                <TextField
                    id="standard-multiline-flexible"
                    multiline
                    autoFocus
                    value={value}
                    onChange={({ target }) => setValue(target.value)}
                    variant="outlined"
                    InputProps={{
                        className: "g-editor-input"
                    }}
                />
            </form>
        )

    else
        return (
            <span
                onClick={() => setIsEditingMode(true)}
                className="g-editor-input"
            >
                {value}
            </span>
        )
}

EditorInput.propTypes = {
    val: PropTypes.string.isRequired,
    editHandler: PropTypes.func.isRequired
}

export default React.memo(EditorInput);
