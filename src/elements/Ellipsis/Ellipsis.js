import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './Ellipsis.css';


function Ellipsis({ wrapperClassName, children }) {

    const parentNode = useRef();

    const [isOpen, setIsOpen] = useState(false)

    const handler = ({ target }) => {

        if (parentNode.current) {

            if (parentNode.current.contains(target))
                return

            setIsOpen(false);
        }

    }

    useEffect(() => {

        if (isOpen)
            document.addEventListener('mousedown', handler);

        else
            document.removeEventListener('mousedown', handler);

    }, [isOpen])

    return (
        <div
            className={"g-ellipsis " + wrapperClassName}
            ref={parentNode}
        >
            <i
                className="fa fa-ellipsis-h"
                onClick={() => setIsOpen(true)}
            ></i>

            {
                isOpen &&
                <div className="g-ellipsis__body">
                    <div className="g-ellipsis__body__h"> Action </div>
                    {children}
                </div>
            }

        </div>
    )

}

Ellipsis.propTypes = {
    children: PropTypes.node.isRequired,
    wrapperClassName: PropTypes.string
}

export default React.memo(Ellipsis);