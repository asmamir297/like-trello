import React from 'react';
import BoardHeader from './BoardHeader/BoardHeader';
import BoardBody from './BoardBody/BoardBody';
import SnackBar from '../elements/SnackBar/SnackBar';
import './App.css';

function App() {
  return (

    <div className="app">

      <SnackBar />

      <BoardHeader />

      <BoardBody />

    </div>

  )
}

export default App;
