import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { editListTitle, deleteList } from '../../store/lists';
import Ellipsis from '../../elements/Ellipsis/Ellipsis';
import EditorInput from '../../elements/EditorInput/EditorInput';
import './ListHeader.css';

function ListHeader({ id, title, deleteList, editListTitle }) {

    const deleteHandler = () => deleteList({ id });
    const editHandler = title => editListTitle({ id, title });

    return (
        <div className="l-column__header">

            <EditorInput
                val={title}
                editHandler={editHandler}
            />

            <Ellipsis wrapperClassName="l-column__header__ellipsis">
                <div onClick={deleteHandler}> Delete </div>
            </Ellipsis>

        </div>
    )

}

ListHeader.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    deleteList: PropTypes.func.isRequired,
    editListTitle: PropTypes.func.isRequired
}

export default connect(undefined, { deleteList, editListTitle })(React.memo(ListHeader));