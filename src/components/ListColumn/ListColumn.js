import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addItem } from '../../store/lists';
import ListHeader from '../ListHeader/ListHeader';
import ListBody from '../ListBody/ListBody';
import AddButt from '../../elements/AddButt/AddButt';
import './ListColumn.css';

function ListColumn({ title, id, addItem }) {

    const addNewColumnHandler = title => addItem({ listId: id, title });

    return (

        <div className="l-column">

            <div className="l-container">

                <ListHeader
                    title={title}
                    id={id}
                />

                <ListBody id={id} />

                <AddButt
                    onSubmit={addNewColumnHandler}
                    text="Add New Task"
                    wrapperClass="l-column__butt"
                />

            </div>

        </div>

    )
}

ListColumn.propTypes = {
    title: PropTypes.string.isRequired,
    addItem: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired
}

export default connect(undefined, { addItem })(ListColumn);