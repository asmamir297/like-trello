import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-beautiful-dnd';
import { dragEnd, getListsSelector } from '../../store/lists';
import ListColumn from '../ListColumn/ListColumn';
import './ListsWrapper.css';

function ListsWrapper({ lists, dragEnd }) {

    const onDragEndHandler = data => {

        const { source, destination } = data;

        // drop outta range
        if (!destination) return;

        // drop in the same position
        if (source.droppableId === destination.droppableId &&
            source.index === destination.index)
            return;

        dragEnd(data);
    }

    return (
        <DragDropContext onDragEnd={onDragEndHandler}>
            <div className="l-wrapper">
                {
                    lists.map(list =>
                        <ListColumn
                            key={list.id}
                            id={list.id}
                            title={list.title}
                        />
                    )
                }
            </div>
        </DragDropContext>
    )

}

ListsWrapper.propTypes = {
    lists: PropTypes.arrayOf(PropTypes.object).isRequired,
    dragEnd: PropTypes.func.isRequired
}

const mapStateToProps = state => {
    return {
        lists: getListsSelector(state)
    }
}

export default connect(mapStateToProps, { dragEnd })(ListsWrapper);