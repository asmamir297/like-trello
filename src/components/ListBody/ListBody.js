import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Droppable } from 'react-beautiful-dnd';
import { getListItemsSelector } from '../../store/lists';
import TaskItem from '../TaskItem/TaskItem';
import './ListBody.css';


function ListBody({ id, tasks }) {

    return (
        <Droppable droppableId={`${id}`} >
            {
                provided => (
                    <div
                        className="l-column__body"
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                    >
                        {
                            tasks.map((task, i) => (
                                <TaskItem
                                    key={task.id}
                                    id={task.id}
                                    listId={id}
                                    title={task.title}
                                    index={i}
                                    createdAt={task.createdAt}
                                    updatedAt={task.updatedAt}
                                />
                            ))
                        }
                        {provided.placeholder}
                    </div>
                )
            }
        </Droppable>
    )

}

ListBody.propTypes = {
    id: PropTypes.string.isRequired,
    tasks: PropTypes.arrayOf(PropTypes.object).isRequired
}

const mapStateToProps = (state, { id }) => ({
    tasks: getListItemsSelector(id)(state)
})

export default connect(mapStateToProps)(ListBody)