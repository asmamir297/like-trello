import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addList } from '../../store/lists';
import ListsWrapper from '../ListsWrapper/ListsWrapper';
import AddButt from '../../elements/AddButt/AddButt';
import './BoardBody.css';

function BoardBody({ addList }) {

    const addNewListHandler = title => addList({ title });

    return (
        <div className="b__body">

            <ListsWrapper />

            <AddButt
                onSubmit={addNewListHandler}
                text="Add New List"
            />

        </div>
    )

}

BoardBody.propTypes = {
    addList: PropTypes.func.isRequired
}

export default connect(undefined, { addList })(BoardBody);