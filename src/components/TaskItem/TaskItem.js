import React from 'react';
import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import { connect } from 'react-redux';
import { deleteItem, editItem } from '../../store/lists';
import EditorInput from '../../elements/EditorInput/EditorInput';
import './TaskItem.css';

function TaskItem({
    id,
    title,
    index,
    listId,
    createdAt,
    updatedAt,
    deleteItem,
    editItem
}) {

    const deleteHandler = () => {
        deleteItem({ listId, id });
    }

    const editHandler = title => {
        editItem({ listId, id, title });
    }

    return (

        <Draggable draggableId={`${id}`} index={index} >
            {
                provided => (
                    <div
                        key={id}
                        className="task-item"
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                    >
                        <div>

                            <EditorInput
                                val={title}
                                editHandler={editHandler}
                            />

                            <i
                                className="fa fa-trash"
                                onClick={deleteHandler}
                            ></i>

                        </div>

                        <div className="task-item__time-stamp">
                            <div>
                                <span> created: </span>
                                <span> {createdAt} </span>
                            </div>

                            <div>
                                <span> updated: </span>
                                <span> {updatedAt} </span>
                            </div>

                        </div>

                    </div>
                )
            }
        </Draggable>

    )
}

TaskItem.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    title: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    listId: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
    deleteItem: PropTypes.func.isRequired,
    editItem: PropTypes.func.isRequired
}

export default connect(undefined, { deleteItem, editItem })(TaskItem);