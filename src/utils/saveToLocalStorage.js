export function saveStateToLocalStorage(lists) {

    try {

        const serializeLists = JSON.stringify(lists);

        localStorage.setItem('boardData', serializeLists);

    } catch (err) {
        new Error(err);
    }

}

export const preLoadStateFromLocalStorage = () => {
    try {

        const serializedState = localStorage.getItem('boardData');

        if (serializedState === null)
            return undefined;

        return JSON.parse(serializedState);

    } catch (err) {
        return undefined;
    }
}