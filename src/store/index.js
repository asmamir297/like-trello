import {
    configureStore,
    getDefaultMiddleware,
    combineReducers
} from '@reduxjs/toolkit';
import throttle from 'lodash/throttle';
import apiMiddleware from './middleware/api';
import toasterMiddleware from './middleware/toaster';
import listsReducer from './lists';
import snackbarReducer from './snackbar';
import {
    saveStateToLocalStorage,
    preLoadStateFromLocalStorage
} from '../utils/saveToLocalStorage'


const preloadedState = preLoadStateFromLocalStorage();

const rootReducer = combineReducers({
    listData: listsReducer,
    snackbar: snackbarReducer
});

const store = configureStore({
    reducer: rootReducer,
    preloadedState,
    middleware: [
        ...getDefaultMiddleware(),
        apiMiddleware,
        toasterMiddleware
    ]
});

store.subscribe(throttle(() => {

    saveStateToLocalStorage({ listData: store.getState().listData });

}, 1000))

export default store;