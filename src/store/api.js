import { createAction } from '@reduxjs/toolkit';

export const apiCall = createAction("api/apiCall");
export const apiCallSuccess = createAction("api/apiCallSuccess");
export const apiCallFailure = createAction("api/apiCallFailure");