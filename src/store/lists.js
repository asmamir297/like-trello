import { createSlice } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import { todosEndpoint } from '../consts';
import { apiCall } from './api';


const listsSlice = createSlice({
    name: "list",
    initialState: {
        lists: {},
        isFetching: false,
        lastFetchAt: null
    },
    reducers: {
        requestLists: state => {
            state.isFetching = true;
        },
        recieveLists: (state, action) => {

            const defaultListId = uuidv4();

            state.isFetching = false;
            state.lastFetchAt = Date.now();

            state.lists = {
                [defaultListId]: {
                    title: "Todo",
                    id: defaultListId,
                    tasks: [{
                        ...action.payload,
                        createdAt: moment().format("M/D/YYYY HH:mm"),
                        updatedAt: moment().format("M/D/YYYY HH:mm"),
                    }]
                }
            }

        },
        failToRecieveLists: (state, action) => {
            state.isFetching = false;
        },
        addList: (state, action) => {

            const listId = uuidv4();

            state.lists[listId] = {
                ...action.payload,
                id: listId,
                tasks: []
            };
        },
        editListTitle: (state, action) => {

            const list = state.lists[action.payload.id];
            list.title = action.payload.title;

        },
        deleteList: (state, action) => {

            delete state.lists[action.payload.id];

        },
        addItem: (state, action) => {
            const list = state.lists[action.payload.listId];

            list.tasks.push({
                id: uuidv4(),
                ...action.payload,
                createdAt: moment().format("M/D/YYYY HH:mm"),
                updatedAt: moment().format("M/D/YYYY HH:mm")
            });
        },
        deleteItem: (state, action) => {

            const list = state.lists[action.payload.listId];

            const taskIndex = list.tasks.findIndex(task => task.id === action.payload.id);

            list.tasks.splice(taskIndex, 1);

        },
        editItem: (state, action) => {

            const list = state.lists[action.payload.listId];

            const task = list.tasks.find(task => task.id === action.payload.id);

            task.title = action.payload.title;
            task.updatedAt = moment().format("M/D/YYYY HH:mm");

        },
        dragEnd: (state, action) => {

            const { source, destination } = action.payload;

            const desList = state.lists[destination.droppableId];
            const srcList = state.lists[source.droppableId];

            // remove from src list
            const [removed] = srcList.tasks.splice(source.index, 1);
            // inject in dest list
            desList.tasks.splice(destination.index, 0, removed);
            removed.updatedAt = moment().format("M/D/YYYY HH:mm");

        }
    }
});


// action creators
export const {
    addList,
    editListTitle,
    deleteList,
    addItem,
    deleteItem,
    editItem,
    dragEnd,
    requestLists,
    recieveLists,
    failToRecieveLists,
} = listsSlice.actions;

// reducer
export default listsSlice.reducer;

// async action
export const getPlaceHolderTasks = () => (dispatch, getState) => {

    const lastFetch = getState().listData.lastFetchAt;

    if (lastFetch)
        return

    dispatch(apiCall({
        url: todosEndpoint,
        onStartAction: requestLists.type,
        onSuccessAction: recieveLists.type,
        onFailureAction: failToRecieveLists.type
    }));
}

// selectors
export const getListsSelector = createSelector(
    state => state.listData.lists,
    lists => Object.values(lists)
);

export const getListItemsSelector = listId => createSelector(
    state => state.listData.lists[listId],
    list => list.tasks
);