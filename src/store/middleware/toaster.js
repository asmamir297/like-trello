import { apiCallFailure, apiCallSuccess } from '../api';
import {
    addItem,
    deleteItem,
    editItem,
    addList,
    deleteList,
    editListTitle
} from '../lists';
import { openSnackbar } from '../snackbar';

const toasterMw = store => next => action => {

    next(action);

    if (action.type === apiCallFailure.type)
        store.dispatch(openSnackbar({ msg: action.payload }));

    if (action.type === apiCallSuccess.type)
        store.dispatch(openSnackbar({ msg: "Data recieved successfully" }));

    if (action.type === addItem.type)
        store.dispatch(openSnackbar({ msg: "Item Added Successfully" }));

    if (action.type === deleteItem.type)
        store.dispatch(openSnackbar({ msg: "Item Deleted Successfully" }));

    if (action.type === editItem.type)
        store.dispatch(openSnackbar({ msg: "Item Title Edited Successfully" }));

    if (action.type === addList.type)
        store.dispatch(openSnackbar({ msg: "List Added Successfully" }));

    if (action.type === deleteList.type)
        store.dispatch(openSnackbar({ msg: "List Deleted Successfully" }));

    if (action.type === editListTitle.type)
        store.dispatch(openSnackbar({ msg: "List Title Edited Successfully" }));

}

export default toasterMw;