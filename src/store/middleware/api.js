import axios from 'axios';
import { apiCall, apiCallSuccess, apiCallFailure } from '../api';
import { baseURL } from '../../consts';

const apiMd = store => next => async action => {

    if (action.type !== apiCall.type)
        return next(action);

    const {
        url,
        onStartAction,
        onSuccessAction,
        onFailureAction,
        apiMethod,
        data
    } = action.payload;

    if (onStartAction)
        store.dispatch({ type: onStartAction });

    next(action);

    try {

        const res = await axios.request({
            baseURL,
            url,
            apiMethod,
            data
        });

        console.log(res);

        if (res.status === 200) {

            store.dispatch(apiCallSuccess(res.data));

            if (onSuccessAction)
                store.dispatch({
                    type: onSuccessAction,
                    payload: res.data
                });

        }

        else {

            store.dispatch(apiCallFailure(res.data.error));

            if (onFailureAction)
                store.dispatch({
                    type: onFailureAction,
                    payload: res.data.error
                });
        }

    } catch (e) {

        store.dispatch(apiCallFailure(e.message));

        if (onFailureAction)
            store.dispatch({
                type: onFailureAction,
                payload: e.message
            });

    }

}

export default apiMd;