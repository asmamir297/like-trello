import { createSlice } from '@reduxjs/toolkit';

const snackbarSlice = createSlice({
    name: 'snackbar',
    initialState: {
        isOpen: false,
        msg: ''
    },
    reducers: {
        openSnackbar: (state, action) => {
            state.isOpen = true;
            state.msg = action.payload.msg;
        },
        clearSnackbar: (state, action) => {
            state.isOpen = false;
        }
    }
})

export const {
    openSnackbar,
    clearSnackbar
} = snackbarSlice.actions;

export default snackbarSlice.reducer;